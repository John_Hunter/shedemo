﻿using Autofac;
using Autofac.Integration.Mvc;
using IncidentReporter.Data.EF;
using IncidentReporter.Domain.Interfaces;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace IncidentReporter.Web
{
	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			var builder = new ContainerBuilder();
			
			builder.RegisterControllers(Assembly.GetExecutingAssembly());
			builder.RegisterType<IncidentReporterDB>().As<IDataContext>().InstancePerRequest();

			var container = builder.Build();
			var resolver = new AutofacDependencyResolver(container);
			DependencyResolver.SetResolver(resolver);
		}
	}
}