﻿using IncidentReporter.Web.Infrastructure.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IncidentReporter.Web
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute("DangerousOccurrences", "dangerous-occurrences/{action}/{id}", defaults: new { controller = "DangerousOccurrence", action = "Index", id = UrlParameter.Optional });
			routes.MapRoute("OccupationalIllness", "occupational-illness/{action}/{id}", defaults: new { controller = "OccupationalIllness", action = "Index", id = UrlParameter.Optional });
			routes.MapRoute("OverseasDisease", "overseas-disease/{action}/{id}", defaults: new { controller = "OverseasDisease", action = "Index", id = UrlParameter.Optional });

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
