﻿using IncidentReporter.Domain.Entities;
using IncidentReporter.Web.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IncidentReporter.Web.Models
{
	public class DangerousOccurrenceFormModel : IncidentFormModelBase, IIncidentForm<DangerousOccurrenceReport>
	{
		[RegularExpression("(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])", ErrorMessage = "You must provide the time of the incident in 24hr format")]
		[DataType(DataType.Time)]
		[DisplayName("Time of incident")]
		[Required(ErrorMessage = "You must provide the time of the incident in 24hr format")]
		public string TimeOfIncident { get; set; }

		[Required(ErrorMessage = "Your must provide the location of the incident")]
		public string Location { get; set; }

		[Required(ErrorMessage = "Your must provide the underlying cause of the dangerous occurrence")]
		[DisplayName("Cause of incident")]
		public string CauseOfDangerousOccurrence { get; set; }


		public void ApplyTo(DangerousOccurrenceReport report, int id = 0)
		{
			report.Id = id;
			report.AddressLine1 = this.AddressLine1;
			report.AddressLine2 = this.AddressLine2;
			report.AffectedPersonStatusId = this.AffectedPersonStatusId;
			report.CauseOfDangerousOccurrence = this.CauseOfDangerousOccurrence;
			report.DateOfIncident = this.DateOfIncident;
			report.Description = this.Description;
			report.Email = this.Email;
			report.FirstName = this.FirstName;
			report.LastName = this.LastName;
			report.Location = this.Location;
			report.NameOfAffectedPerson = this.NameOfAffectedPerson;
			report.Postcode = this.Postcode;
			report.Telephone = this.Telephone;
			report.TimeOfIncident = this.TimeOfIncident;
			report.TitleId = this.TitleId;
			report.Town = this.Town;
		}


		public void UpdateFrom(DangerousOccurrenceReport report)
		{
			this.Id = report.Id;
			this.AddressLine1 = report.AddressLine1;
			this.AddressLine2 = report.AddressLine2;
			this.AffectedPersonStatusId = report.AffectedPersonStatusId;
			this.CauseOfDangerousOccurrence = report.CauseOfDangerousOccurrence;
			this.DateOfIncident = report.DateOfIncident;
			this.Description = report.Description;
			this.Email = report.Email;
			this.FirstName = report.FirstName;
			this.LastName = report.LastName;
			this.Location = report.Location;
			this.NameOfAffectedPerson = report.NameOfAffectedPerson;
			this.SubmissionTimestamp = report.SubmissionTimestamp;
			this.Postcode = report.Postcode;
			this.Telephone = report.Telephone;
			this.TimeOfIncident = report.TimeOfIncident;
			this.TitleId = report.TitleId;
			this.Town = report.Town;
		}

	}
}