﻿using IncidentReporter.Domain.Entities;
using IncidentReporter.Web.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IncidentReporter.Web.Models
{
	public class IncidentListPageModel
	{
		public IncidentListPageModel(IEnumerable<IncidentReport> incidents, int currentPage)
		{
			IncidentList = incidents ?? new IncidentReport[0];
			CurrentPage = currentPage;
			var count = incidents.Count();
			TotalPages = count / Paging.PAGE_SIZE;
		}

		public IEnumerable<IncidentReport> IncidentList { get; set; }

		public int CurrentPage { get; set; }

		public int TotalPages { get; set; }
	}
}