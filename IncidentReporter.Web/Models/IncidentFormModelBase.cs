﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace IncidentReporter.Web.Models
{
	public class IncidentFormModelBase 
	{
		public List<SelectListItem> TitleList { get; set; }
		public List<SelectListItem> AffectedPersonStatusList { get; set; }

		public int Id { get; set; }

		[Required(ErrorMessage = "Please complete the Title field")]
		[DisplayName("Title")]
		public int TitleId { get; set; }

		[Required(ErrorMessage = "Please provide the first name of the Responsible Person")]
		[DisplayName("First Name")]
		public string FirstName { get; set; }

		[Required(ErrorMessage = "Please provide the last name of the Responsible Person")]
		[DisplayName("Last Name")]
		public string LastName { get; set; }

		[Required(ErrorMessage = "Please provide the first line of the contact address for the Responsible Person")]
		[DisplayName("Address 1")]
		public string AddressLine1 { get; set; }

		[DisplayName("Address 2")]
		public string AddressLine2 { get; set; }

		[Required(ErrorMessage = "Please provide the town of the contact address for the Responsible Person")]
		public string Town { get; set; }

		[Required(ErrorMessage = "Please provide the postcode of the contact address for the Responsible Person")]
		public string Postcode { get; set; }

		[RegularExpression(@"\d{11}", ErrorMessage = "You must provide a valid 11 digit phone number")]
		public string Telephone { get; set; }

		[DataType(DataType.EmailAddress)]
		[RegularExpression(@".+@.+\..+", ErrorMessage = "You must provide a valid email address")]
		public string Email { get; set; }

		[Required(ErrorMessage = "You must provide the date on which the incident occurred")]
		[DisplayName("Date of incident")]
		[DataType(DataType.Date)]
		public DateTime DateOfIncident { get; set; }

		[Required(ErrorMessage = "You must provide a description of the incident occurred")]
		public string Description { get; set; }

		[Required(ErrorMessage = "You must provide the name of the person affected by the incident")]
		[DisplayName("Name of affected person")]
		public string NameOfAffectedPerson { get; set; }

		/// <summary>
		/// For work injuries this refers to the affected person's Occupation as per RIDDOR
		/// For non-work injuries this refers to the affected person's Status (e.g. by stander, passenger, etc) as per RIDDOR
		/// </summary>
		[DisplayName("Affected person status")]
		public int AffectedPersonStatusId { get; set; }

		[DisplayName("Employee?")]
		public virtual bool? AffectedPersonIsEmployee { get; set; }
	
		public DateTime? SubmissionTimestamp { get; internal set; }


		internal void SetAffectedPersonStatus(bool isEmployee)
		{
			if (AffectedPersonIsEmployee != isEmployee)
			{
				AffectedPersonIsEmployee = isEmployee;
				AffectedPersonStatusId = 0;
			}
		}
	}
}
