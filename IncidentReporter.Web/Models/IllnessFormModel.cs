﻿using IncidentReporter.Domain.Entities;
using IncidentReporter.Web.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IncidentReporter.Web.Models
{
	public class IllnessFormModel : IncidentFormModelBase, IIncidentForm<IllnessReport>
	{
		[DisplayName("Diagnosis")]
		public int InjuryId { get; set; }

		public List<SelectListItem> InjuryTypeList { get; set; }


		public void ApplyTo(IllnessReport report, int id)
		{
			report.Id = id;
			report.AddressLine1 = this.AddressLine1;
			report.AddressLine2 = this.AddressLine2;
			report.AffectedPersonStatusId = this.AffectedPersonStatusId;
			report.DateOfIncident = this.DateOfIncident;
			report.Description = this.Description;
			report.Email = this.Email;
			report.FirstName = this.FirstName;
			report.LastName = this.LastName;
			report.NameOfAffectedPerson = this.NameOfAffectedPerson;
			report.Postcode = this.Postcode;
			report.Telephone = this.Telephone;
			report.TitleId = this.TitleId;
			report.Town = this.Town;
			report.DiagnosisId = this.InjuryId;
		}

		public void UpdateFrom(IllnessReport report)
		{

			this.Id = report.Id;
			this.AddressLine1 = report.AddressLine1;
			this.AddressLine2 = report.AddressLine2;
			this.AffectedPersonStatusId = report.AffectedPersonStatusId;
			this.DateOfIncident = report.DateOfIncident;
			this.Description = report.Description;
			this.Email = report.Email;
			this.FirstName = report.FirstName;
			this.LastName = report.LastName;
			this.NameOfAffectedPerson = report.NameOfAffectedPerson;
			this.Postcode = report.Postcode;
			this.Telephone = report.Telephone;
			this.TitleId = report.TitleId;
			this.Town = report.Town;
			this.InjuryId = report.DiagnosisId;
		}
	}
}