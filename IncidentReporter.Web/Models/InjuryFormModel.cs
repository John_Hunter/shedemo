﻿using IncidentReporter.Domain.Entities;
using IncidentReporter.Web.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IncidentReporter.Web.Models
{
	public class InjuryFormModel : IncidentFormModelBase, IIncidentForm<InjuryReport>
	{
		[RegularExpression("(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])", ErrorMessage = "You must provide the time of the injury in 24hr format")]
		[MaxLength(length: 5)]
		public string TimeOfIncident { get; set; }

		[Required(ErrorMessage = "Your must provide the location of the injury")]
		public string Location { get; set; }

		[DisplayName("Period of absence (days)")]
		public int PeriodOfAbsence { get; set; }

		[DisplayName("Type of injury")]
		public int InjuryId { get; set; }

		public List<SelectListItem> InjuryTypeList { get; set; }


		public void ApplyTo(InjuryReport report, int id)
		{
			report.Id = id;
			report.AddressLine1 = this.AddressLine1;
			report.AddressLine2 = this.AddressLine2;
			report.AffectedPersonStatusId = this.AffectedPersonStatusId;
			report.DateOfIncident = this.DateOfIncident;
			report.Description = this.Description;
			report.Email = this.Email;
			report.FirstName = this.FirstName;
			report.LastName = this.LastName;
			report.Location = this.Location;
			report.NameOfAffectedPerson = this.NameOfAffectedPerson;
			report.Postcode = this.Postcode;
			report.Telephone = this.Telephone;
			report.TimeOfIncident = this.TimeOfIncident;
			report.TitleId = this.TitleId;
			report.Town = this.Town;
			report.PeriodOfAbsence = this.PeriodOfAbsence;
			report.InjuryTypeId = this.InjuryId;
		}

		public void UpdateFrom(InjuryReport report)
		{

			this.Id = report.Id;
			this.AddressLine1 = report.AddressLine1;
			this.AddressLine2 = report.AddressLine2;
			this.AffectedPersonStatusId = report.AffectedPersonStatusId;
			this.DateOfIncident = report.DateOfIncident;
			this.Description = report.Description;
			this.Email = report.Email;
			this.FirstName = report.FirstName;
			this.LastName = report.LastName;
			this.Location = report.Location;
			this.NameOfAffectedPerson = report.NameOfAffectedPerson;
			this.Postcode = report.Postcode;
			this.Telephone = report.Telephone;
			this.TimeOfIncident = report.TimeOfIncident;
			this.TitleId = report.TitleId;
			this.Town = report.Town;
			this.PeriodOfAbsence = report.PeriodOfAbsence;
			this.InjuryId = report.InjuryTypeId;
		}
	}
}