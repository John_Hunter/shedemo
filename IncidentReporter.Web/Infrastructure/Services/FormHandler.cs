﻿using IncidentReporter.Domain.Entities;
using IncidentReporter.Domain.Interfaces;
using IncidentReporter.Web.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IncidentReporter.Web.Infrastructure.Services
{
	public class FormHandler<T> where T : IncidentReport
	{
		private Domain.Interfaces.IDataContext _dataContext;
		private ControllerContext _controllerContext;

		protected IReportRepository ReportRepository
		{
			get
			{
				return _dataContext;
			}
		}

		public FormHandler(IDataContext dataContext, ControllerContext controllerContext)
		{
			// TODO: Complete member initialization
			this._dataContext = dataContext;
			this._controllerContext = controllerContext;
		}

		internal ActionResult FormAction(IIncidentForm<T> formModel, string buttonAction)
		{
			ActionResult result = null;
			var report = (T)ReportRepository.Get(formModel.Id);
			if (report == null)
			{
				if (formModel.Id == 0)
				{
					// no report found because it's a new one
					report = Activator.CreateInstance<T>();
					ReportRepository.Add(report);
					result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Edit" }, { "controller", _controllerContext.RouteData.Values["controller"] } });
				}
				else
				{
					// should have found it but didn't
					result = new HttpStatusCodeResult(404);
				}
			}
			else
			{
				result = new ViewResult();
				((ViewResult)result).ViewData.Model = formModel;
			}
			formModel.ApplyTo(report, formModel.Id);
			if (buttonAction == "Submit")
			{
				// Domain object is in charge of it's own business logic as per DDD principles
				report.MarkSubmitted();
				_controllerContext.Controller.TempData["Success"] = _controllerContext.Controller.ViewData["IncidentType"] + " report saved and submitted successfully";
			}
			else
			{
				_controllerContext.Controller.TempData["Success"] = _controllerContext.Controller.ViewData["IncidentType"] + " report saved successfully";
			}
			if (result is ViewResult)
			{
				((ViewResult)result).TempData["Success"] = _controllerContext.Controller.TempData["Success"];
			}
			ReportRepository.SaveChanges();

			// reflect any DB changes in the UI, also capture the Id of any newly created record
			formModel.UpdateFrom(report);
			return result;
		}
	}
}