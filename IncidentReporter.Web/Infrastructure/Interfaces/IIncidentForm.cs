﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Web.Infrastructure.Interfaces
{
	public interface IIncidentForm<T>
	{
		int Id { get; set; }
		void ApplyTo(T report, int id);
		void UpdateFrom(T report);
	}
}
