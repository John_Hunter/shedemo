﻿using IncidentReporter.Data.EF;
using IncidentReporter.Domain.Interfaces;
using IncidentReporter.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IncidentReporter.Web.Infrastructure.MVC
{
	public class IncidentBaseController : Controller
	{
		protected IDataContext DataContext { get; set; }

		// this would be better handled using IoC
		protected IReportRepository ReportRepository
		{
			get
			{
				if (DataContext == null)
				{
					DataContext = new IncidentReporterDB();
				}
				return DataContext;
			}
		}

		protected ILookupRepository LookupRepository
		{
			get
			{
				if (DataContext == null)
				{
					DataContext = new IncidentReporterDB();
				}
				return DataContext;
			}
		}

		protected IncidentBaseController()
		{

		}

		public IncidentBaseController(IDataContext dataContext)
		{
			DataContext = dataContext;
		}

		protected virtual void PopulateListOptions(IncidentFormModelBase viewModel)
		{
			var titles = LookupRepository.GetTitleList();
			viewModel.TitleList = titles.Select(t => new SelectListItem { Value = t.Id.ToString(), Text = t.TitleText }).ToList();
			viewModel.TitleList.Insert(0, new SelectListItem { Value = string.Empty, Text = "Please select..." });

			var affectedPersonStatusList = LookupRepository.GetAffectedPersonStatusList();
			var affectedPersonStatus = affectedPersonStatusList.FirstOrDefault(x => x.Id == viewModel.AffectedPersonStatusId);
			if (affectedPersonStatus != null)
			{
				viewModel.AffectedPersonIsEmployee = affectedPersonStatus.IsEmployeeStatus;
			}

			viewModel.AffectedPersonStatusList = affectedPersonStatusList.Where(s => s.IsEmployeeStatus == viewModel.AffectedPersonIsEmployee)
				.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.StatusText }).ToList();
			viewModel.AffectedPersonStatusList.Insert(0, new SelectListItem { Value = string.Empty, Text = "Please select..." });
		}
	}
}