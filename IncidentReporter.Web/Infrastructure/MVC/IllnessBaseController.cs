﻿using IncidentReporter.Web.Infrastructure.Services;
using IncidentReporter.Domain.Entities;
using IncidentReporter.Web.Infrastructure.MVC;
using IncidentReporter.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IncidentReporter.Web.Infrastructure.Constants;

namespace IncidentReporter.Web.Infrastructure.MVC
{
	public class IllnessBaseController : IncidentBaseController
	{
		protected Domain.Enums.InjuryCategoryEnum _illnesCategory;


		public ActionResult Index(int page = 1)
		{
			using (ReportRepository)
			{
				var allIncidents = ReportRepository.All().OfType<IllnessReport>().Where(r => r.Diagnosis.InjuryCategory == _illnesCategory);
				var totalIncidents = allIncidents.Count();
				var pageIncidents = totalIncidents == 0 ?
					new IncidentReport[0].AsQueryable() :
					allIncidents.OrderByDescending(r => r.CreatedOn).Skip((page - 1) * Paging.PAGE_SIZE).Take(Paging.PAGE_SIZE);
				var viewModel = new IncidentListPageModel(pageIncidents.ToList(), page);
				return View("../Illness/Index", viewModel);
			}
		}


		public ActionResult Create()
		{
			using (LookupRepository)
			{
				var titles = LookupRepository.GetTitleList();
				var viewModel = new IllnessFormModel { AffectedPersonIsEmployee = true };
				PopulateListOptions(viewModel);
				return View("../Illness/Create", viewModel);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(IllnessFormModel formModel, string EmployeeSwitch = "", string buttonAction = "")
		{
			var result = FormAction(formModel, EmployeeSwitch, buttonAction);
			if(result is ViewResult)
			{
				// need to specify the path to the view as it is a custom shared folder
				((ViewResult)result).ViewName = "../Illness/Create";
			}
			return result ?? View("../Illness/Create", formModel);
		}

		public ActionResult Edit(int id)
		{
			IllnessReport report = null;
			using (ReportRepository)
			{
				report = ReportRepository.Get(id) as IllnessReport;
				if (report == null)
				{
					return new HttpStatusCodeResult(404);
				}
				var viewModel = new IllnessFormModel { AffectedPersonIsEmployee = true };
				viewModel.UpdateFrom(report);
				PopulateListOptions(viewModel);
				return View("../Illness/Edit", viewModel);
			}

		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(IllnessFormModel formModel, string EmployeeSwitch = "", string buttonAction = "")
		{
			var result = FormAction(formModel, EmployeeSwitch, buttonAction);
			if (result is ViewResult)
			{
				// need to specify the path to the view as it is a custom shared folder
				((ViewResult)result).ViewName = "../Illness/Edit";
			} 
			return result ?? View("../Illness/Edit", formModel);
		}


		public ActionResult Delete(int id)
		{
			IllnessReport report = null;
			using (ReportRepository)
			{
				report = ReportRepository.Get(id) as IllnessReport;
				if (report == null)
				{
					return new HttpStatusCodeResult(404);
				}
				var viewModel = new IllnessFormModel { AffectedPersonIsEmployee = true };
				viewModel.UpdateFrom(report);
				PopulateListOptions(viewModel);
				return View("../Illness/Delete", viewModel);
			}

		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id, string confirmDelete = "")
		{
			using (ReportRepository)
			{
				var viewModel = new IllnessFormModel();
				var report = ReportRepository.Get(id) as IllnessReport;
				// reflect any DB changes in the UI
				viewModel.UpdateFrom(report);
				if (confirmDelete == "on")
				{
					ReportRepository.Delete(report);
					ReportRepository.SaveChanges();
					return RedirectToAction("Index");
				}
				PopulateListOptions(viewModel);
				return View("../Illness/Delete", viewModel);
			}
		}

		private ActionResult FormAction(IllnessFormModel formModel, string EmployeeSwitch, string buttonAction)
		{
			ActionResult result = null;
			using (ReportRepository)
			{
				var handler = new FormHandler<IllnessReport>(base.DataContext, this.ControllerContext);
				if (!string.IsNullOrWhiteSpace(buttonAction))
				{
					if (ModelState.IsValid)
					{
						result = handler.FormAction(formModel, buttonAction);
					}
				}
				else
				{
					if (!string.IsNullOrWhiteSpace(EmployeeSwitch))
					{
						ModelState.Clear();
						formModel.SetAffectedPersonStatus((EmployeeSwitch == "1"));
					}
				}
				PopulateListOptions(formModel);
				if (result is RedirectToRouteResult)
				{
					// this will be hit if the form has been submitted by creating a new DO report
					((RedirectToRouteResult)result).RouteValues.Add("id", formModel.Id);
				}
			}
			return result;
		}

		protected override void PopulateListOptions(IncidentFormModelBase viewModel)
		{
			var injuryModel = viewModel as IllnessFormModel;
			injuryModel.InjuryTypeList = LookupRepository.GetInjuryTypes()
				.Where(i => i.InjuryCategory == _illnesCategory)
				.Select(i => new SelectListItem { Value = i.Id.ToString(), Text = i.InjuryTypeText }).ToList();
			injuryModel.InjuryTypeList.Insert(0, new SelectListItem { Value = string.Empty, Text = "Please select..." });

			base.PopulateListOptions(injuryModel);
		}
	}
}