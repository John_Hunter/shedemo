﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IncidentReporter.Web.Infrastructure.Constants
{
	public class Paging
	{
		public const int PAGE_SIZE = 5;
	}
}