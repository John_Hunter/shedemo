﻿using IncidentReporter.Data.EF;
using IncidentReporter.Web;
using IncidentReporter.Web.Infrastructure.MVC;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace IncidentReporter.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
		protected void Application_Start()
		{
			Database.SetInitializer<IncidentReporterDB>(new DropCreateDatabaseIfModelChanges<IncidentReporterDB>());
			AreaRegistration.RegisterAllAreas();
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
		}
    }
}
