﻿using IncidentReporter.Web.Infrastructure.Services;
using IncidentReporter.Data.EF;
using IncidentReporter.Domain.Entities;
using IncidentReporter.Domain.Interfaces;
using IncidentReporter.Web.Infrastructure.MVC;
using IncidentReporter.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IncidentReporter.Web.Infrastructure.Constants;

namespace IncidentReporter.Web.Controllers
{
	public class DangerousOccurrenceController : IncidentBaseController
	{

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			ViewData["IncidentType"] = "Dangerous Occurrence Report ";
			base.OnActionExecuting(filterContext);
		}

		public DangerousOccurrenceController()
		{

		}

		public DangerousOccurrenceController(IDataContext dbContext)
		{
			this.DataContext = dbContext;
		}

		public ActionResult Index(int page = 1)
		{
			using (ReportRepository)
			{
				var allIncidents = ReportRepository.All().OfType<DangerousOccurrenceReport>();
				var totalIncidents = allIncidents.Count();
				var pageIncidents = totalIncidents == 0 ?
					new IncidentReport[0].AsQueryable() :
					allIncidents.OrderByDescending(r => r.CreatedOn).Skip((page - 1) * Paging.PAGE_SIZE).Take(Paging.PAGE_SIZE);
				var viewModel = new IncidentListPageModel(pageIncidents.ToList(), page);
				return View(viewModel);
			}
		}


		public ActionResult Create()
		{
			using (LookupRepository)
			{
				var titles = LookupRepository.GetTitleList();
				var viewModel = new DangerousOccurrenceFormModel();
				PopulateListOptions(viewModel);
				return View(viewModel);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(DangerousOccurrenceFormModel formModel, string EmployeeSwitch = "", string buttonAction = "")
		{
			var result = FormAction(formModel, EmployeeSwitch, buttonAction);
			return result ?? View(formModel);
		}

		public ActionResult Edit(int id)
		{
			DangerousOccurrenceReport report = null;
			using (ReportRepository)
			{
				report = ReportRepository.Get(id) as DangerousOccurrenceReport;
				if (report == null)
				{
					return new HttpStatusCodeResult(404);
				}
				var viewModel = new DangerousOccurrenceFormModel();
				viewModel.UpdateFrom(report);
				PopulateListOptions(viewModel);
				return View(viewModel);
			}

		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(DangerousOccurrenceFormModel formModel, string EmployeeSwitch = "", string buttonAction = "")
		{
			var result = FormAction(formModel, EmployeeSwitch, buttonAction);
			return result ?? View(formModel);
		}

		public ActionResult Delete(int id)
		{
			DangerousOccurrenceReport report = null;
			using (ReportRepository)
			{
				report = ReportRepository.Get(id) as DangerousOccurrenceReport;
				if (report == null)
				{
					return new HttpStatusCodeResult(404);
				}
				var viewModel = new DangerousOccurrenceFormModel();
				viewModel.UpdateFrom(report);
				PopulateListOptions(viewModel);
				return View(viewModel);
			}

		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id, string confirmDelete = "")
		{
			using (ReportRepository)
			{
				var viewModel = new DangerousOccurrenceFormModel();
				var report = ReportRepository.Get(id) as DangerousOccurrenceReport;
				// reflect any DB changes in the UI
				viewModel.UpdateFrom(report);
				if (confirmDelete == "on")
				{
					ReportRepository.Delete(report);
					ReportRepository.SaveChanges();
					TempData["Success"] = "Dangerous Occurrence report " + id + " deleted successfully";
					return RedirectToAction("Index");
				}
				PopulateListOptions(viewModel);
				return View(viewModel);
			}
		}

		private ActionResult FormAction(DangerousOccurrenceFormModel formModel, string EmployeeSwitch, string buttonAction)
		{
			ActionResult result = null;
			using (ReportRepository)
			{
				var handler = new FormHandler<DangerousOccurrenceReport>(base.DataContext, this.ControllerContext);
				if (!string.IsNullOrWhiteSpace(buttonAction))
				{
					if (ModelState.IsValid)
					{
						result = handler.FormAction(formModel, buttonAction);
					}
				}
				else
				{
					if (!string.IsNullOrWhiteSpace(EmployeeSwitch))
					{
						ModelState.Clear();
						formModel.SetAffectedPersonStatus((EmployeeSwitch == "1"));
					}
				}
				PopulateListOptions(formModel);
				if (result is RedirectToRouteResult)
				{
					// this will be hit if the form has been submitted by creating a new DO report
					((RedirectToRouteResult)result).RouteValues.Add("id", formModel.Id);
				}
			}
			return result;
		}


	}
}