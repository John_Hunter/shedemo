﻿using IncidentReporter.Web.Infrastructure.Services;
using IncidentReporter.Domain.Entities;
using IncidentReporter.Web.Infrastructure.MVC;
using IncidentReporter.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IncidentReporter.Web.Controllers
{
	public class OverseasDiseaseController : IllnessBaseController
	{
		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			ViewData["IncidentType"] = "Overseas Disease";
			base._illnesCategory = Domain.Enums.InjuryCategoryEnum.OverseasDisease;
			base.OnActionExecuting(filterContext);
		}

	}
}