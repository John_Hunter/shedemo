﻿using IncidentReporter.Domain.Entities;
using IncidentReporter.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Data.EF
{
	public partial class IncidentReporterDB : IReportRepository
	{
		public void Add(IncidentReport report)
		{
			if(report.Id != 0)
			{
				throw new InvalidOperationException();
			}
			report.CreatedOn = DateTime.Now;
			this.IncidentReports.Add(report);
		}

		public IncidentReport Get(int id)
		{
			var report = this.IncidentReports.FirstOrDefault(r => r.Id == id);
			return report;
		}

		public IQueryable<IncidentReport> All()
		{
			var all = this.IncidentReports;
			return all;
		}


		public void Delete(IncidentReport report)
		{
			this.IncidentReports.Remove(report);
		}
	}
}
