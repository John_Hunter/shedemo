﻿using IncidentReporter.Domain.Interfaces;
using IncidentReporter.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Data.EF
{
	public partial class IncidentReporterDB : ILookupRepository
	{

		public IEnumerable<Title> GetTitleList()
		{
			var titles = this.Set<Title>().ToList();
			return titles;
		}

		public IEnumerable<AffectedPersonStatus> GetAffectedPersonStatusList()
		{
			var statusList = this.Set<AffectedPersonStatus>().ToList();
			return statusList;
		}

		public IEnumerable<InjuryType> GetInjuryTypes()
		{
			var injuryTypeList = this.Set<InjuryType>().ToList();
			return injuryTypeList;
		}
	}
}
