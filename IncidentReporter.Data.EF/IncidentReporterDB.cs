﻿using IncidentReporter.Domain.Entities;
using IncidentReporter.Domain.Interfaces;
using IncidentReporter.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Data.EF
{
	public partial class IncidentReporterDB : DbContext, IDataContext
	{
		public DbSet<IncidentReport> IncidentReports { get; set; }
		public DbSet<Title> Titles { get; set; }
		public DbSet<AffectedPersonStatus> AffectedPersonStatuses { get; set; }
		public DbSet<InjuryType> InjuryTypes { get; set; }

		public IncidentReporterDB()
			: base("name=IncidentReporterDB")
		{
		}

		public new void SaveChanges()
		{
			base.SaveChanges();
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<DangerousOccurrenceReport>().ToTable("DangerousOccurrenceReports");
			modelBuilder.Entity<InjuryReport>().ToTable("InjuryReports");
			modelBuilder.Entity<FatalityReport>().ToTable("FatalityReports");
			modelBuilder.Entity<IllnessReport>().ToTable("IllnessReports");

			modelBuilder.Entity<IllnessReport>()
				.HasRequired(r => r.Diagnosis)
				.WithMany()
				.HasForeignKey(r => r.DiagnosisId);
		}
	}
}
