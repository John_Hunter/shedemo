namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TimeOfIncidentLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DangerousOccurrences", "TimeOfIncident", c => c.String(maxLength: 5));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DangerousOccurrences", "TimeOfIncident", c => c.String());
        }
    }
}
