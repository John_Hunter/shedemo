namespace IncidentReporter.Data.EF.Migrations
{
	using IncidentReporter.Domain.Enums;
	using IncidentReporter.Domain.ValueObjects;
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;

	internal sealed class Configuration : DbMigrationsConfiguration<IncidentReporter.Data.EF.IncidentReporterDB>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
			ContextKey = "IncidentReporter.Data.EF.IncidentReporterDB";
		}

		protected override void Seed(IncidentReporter.Data.EF.IncidentReporterDB context)
		{
			if (!context.Titles.Any())
			{
				context.Titles.AddOrUpdate(new Title { TitleText = "Mr" });
				context.Titles.AddOrUpdate(new Title { TitleText = "Mrs" });
				context.Titles.AddOrUpdate(new Title { TitleText = "Miss" });
				context.Titles.AddOrUpdate(new Title { TitleText = "Dr" });
			}

			if (!context.AffectedPersonStatuses.Any())
			{
				context.AffectedPersonStatuses.AddOrUpdate(new AffectedPersonStatus { IsEmployeeStatus = true, StatusText = "Programmer" });
				context.AffectedPersonStatuses.AddOrUpdate(new AffectedPersonStatus { IsEmployeeStatus = true, StatusText = "Cleaner" });
				context.AffectedPersonStatuses.AddOrUpdate(new AffectedPersonStatus { IsEmployeeStatus = true, StatusText = "Administrator" });

				context.AffectedPersonStatuses.AddOrUpdate(new AffectedPersonStatus { IsEmployeeStatus = false, StatusText = "Passenger" });
				context.AffectedPersonStatuses.AddOrUpdate(new AffectedPersonStatus { IsEmployeeStatus = false, StatusText = "Customer" });
				context.AffectedPersonStatuses.AddOrUpdate(new AffectedPersonStatus { IsEmployeeStatus = false, StatusText = "Visitor" });
			}

			if (!context.InjuryTypes.Any())
			{

				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.Fatality, InjuryTypeText = "Fatality" });

				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.Injury, InjuryTypeText = "Fracture" });
				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.Injury, InjuryTypeText = "Amputation" });
				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.Injury, InjuryTypeText = "Loss of consciousness" });

				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.OccupationalIllness, InjuryTypeText = "Carpal Tunnel Syndrom" });
				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.OccupationalIllness, InjuryTypeText = "Asthma" });
				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.OccupationalIllness, InjuryTypeText = "Tendonitis" });

				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.OverseasDisease, InjuryTypeText = "Cholera" });
				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.OverseasDisease, InjuryTypeText = "Malaria" });
				context.InjuryTypes.AddOrUpdate(new InjuryType { InjuryCategory = InjuryCategoryEnum.OverseasDisease, InjuryTypeText = "Rubella" });
			}
			context.SaveChanges();
		}
	}
}
