namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IncidentReports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TitleId = c.Int(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        AddressLine1 = c.String(nullable: false),
                        AddressLine2 = c.String(),
                        Town = c.String(nullable: false),
                        Postcode = c.String(nullable: false),
                        Telephone = c.String(),
                        Email = c.String(),
                        DateOfIncident = c.DateTime(nullable: false),
                        Description = c.String(nullable: false),
                        NameOfAffectedPerson = c.String(nullable: false),
                        AffectedPersonStatusId = c.Int(nullable: false),
                        ReportStatus = c.Int(nullable: false),
                        SubmissionTimestamp = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DangerousOccurrences",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TimeOfIncident = c.String(),
                        Location = c.String(nullable: false),
                        CauseOfDangerousOccurrence = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncidentReports", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DangerousOccurrences", "Id", "dbo.IncidentReports");
            DropIndex("dbo.DangerousOccurrences", new[] { "Id" });
            DropTable("dbo.DangerousOccurrences");
            DropTable("dbo.IncidentReports");
        }
    }
}
