namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OverseasDiseases : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InjuryTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InjuryCategory = c.Int(nullable: false),
                        InjuryTypeText = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.InjuryTypes");
        }
    }
}
