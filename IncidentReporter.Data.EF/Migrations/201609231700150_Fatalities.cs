namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fatalities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FatalityReports",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TimeOfIncident = c.String(maxLength: 5),
                        Location = c.String(nullable: false),
                        InjuryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncidentReports", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FatalityReports", "Id", "dbo.IncidentReports");
            DropIndex("dbo.FatalityReports", new[] { "Id" });
            DropTable("dbo.FatalityReports");
        }
    }
}
