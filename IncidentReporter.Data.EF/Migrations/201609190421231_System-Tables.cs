namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SystemTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AffectedPersonStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsEmployeeStatus = c.Boolean(nullable: false),
                        StatusText = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Titles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TitleText = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Titles");
            DropTable("dbo.AffectedPersonStatus");
        }
    }
}
