namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Illness : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IllnessReports",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        DiagnosisId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncidentReports", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IllnessReports", "Id", "dbo.IncidentReports");
            DropIndex("dbo.IllnessReports", new[] { "Id" });
            DropTable("dbo.IllnessReports");
        }
    }
}
