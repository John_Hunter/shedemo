namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Diagnosis : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.IllnessReports", "DiagnosisId");
            AddForeignKey("dbo.IllnessReports", "DiagnosisId", "dbo.InjuryTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IllnessReports", "DiagnosisId", "dbo.InjuryTypes");
            DropIndex("dbo.IllnessReports", new[] { "DiagnosisId" });
        }
    }
}
