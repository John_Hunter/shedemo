namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InjuryReports : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.DangerousOccurrences", newName: "DangerousOccurrenceReports");
            CreateTable(
                "dbo.InjuryReports",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        InjuryType_Id = c.Int(nullable: false),
                        TimeOfIncident = c.String(maxLength: 5),
                        Location = c.String(nullable: false),
                        PeriodOfAbsence = c.Int(nullable: false),
                        InjuryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncidentReports", t => t.Id)
                .ForeignKey("dbo.InjuryTypes", t => t.InjuryType_Id, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.InjuryType_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InjuryReports", "InjuryType_Id", "dbo.InjuryTypes");
            DropForeignKey("dbo.InjuryReports", "Id", "dbo.IncidentReports");
            DropIndex("dbo.InjuryReports", new[] { "InjuryType_Id" });
            DropIndex("dbo.InjuryReports", new[] { "Id" });
            DropTable("dbo.InjuryReports");
            RenameTable(name: "dbo.DangerousOccurrenceReports", newName: "DangerousOccurrences");
        }
    }
}
