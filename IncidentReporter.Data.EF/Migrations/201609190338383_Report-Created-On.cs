namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportCreatedOn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IncidentReports", "CreatedOn", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.IncidentReports", "CreatedOn");
        }
    }
}
