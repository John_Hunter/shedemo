namespace IncidentReporter.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveInjuryTypeRelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InjuryReports", "InjuryType_Id", "dbo.InjuryTypes");
            DropIndex("dbo.InjuryReports", new[] { "InjuryType_Id" });
            AddColumn("dbo.InjuryReports", "InjuryTypeId", c => c.Int(nullable: false));
            DropColumn("dbo.InjuryReports", "InjuryType_Id");
            DropColumn("dbo.InjuryReports", "InjuryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InjuryReports", "InjuryId", c => c.Int(nullable: false));
            AddColumn("dbo.InjuryReports", "InjuryType_Id", c => c.Int(nullable: false));
            DropColumn("dbo.InjuryReports", "InjuryTypeId");
            CreateIndex("dbo.InjuryReports", "InjuryType_Id");
            AddForeignKey("dbo.InjuryReports", "InjuryType_Id", "dbo.InjuryTypes", "Id", cascadeDelete: true);
        }
    }
}
