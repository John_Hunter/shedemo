﻿using IncidentReporter.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.Interfaces
{
	public interface IReportRepository : IDisposable
	{
		void Add(IncidentReport report);

		IncidentReport Get(int id);

		void SaveChanges();

		IQueryable<IncidentReport> All();

		void Delete(IncidentReport report);
	}
}
