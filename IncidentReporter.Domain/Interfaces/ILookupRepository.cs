﻿using IncidentReporter.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.Interfaces
{
	public interface ILookupRepository : IDisposable
	{
		IEnumerable<Title> GetTitleList();

		IEnumerable<AffectedPersonStatus> GetAffectedPersonStatusList();

		IEnumerable<InjuryType> GetInjuryTypes();
	}
}
