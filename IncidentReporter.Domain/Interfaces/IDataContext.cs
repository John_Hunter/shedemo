﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.Interfaces
{
	public interface IDataContext : IReportRepository, ILookupRepository
	{
		void SaveChanges();
	}
}
