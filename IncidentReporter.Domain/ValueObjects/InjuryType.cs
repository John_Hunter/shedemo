﻿using IncidentReporter.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.ValueObjects
{
	public class InjuryType
	{
		public int Id { get; set; }
		public InjuryCategoryEnum InjuryCategory { get; set; }
		public string InjuryTypeText { get; set; }
	}
}
