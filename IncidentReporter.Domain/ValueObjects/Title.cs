﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.ValueObjects
{
	public class Title
	{
		public Title()
		{

		}

		public int Id { get; set; }

		public string TitleText { get; set; }
	}
}
