﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.ValueObjects
{
	public class AffectedPersonStatus
	{
		public int Id { get; set; }
		public bool IsEmployeeStatus { get; set; }
		public string StatusText { get; set; }
	}
}
