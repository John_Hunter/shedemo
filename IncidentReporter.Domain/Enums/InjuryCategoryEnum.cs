﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.Enums
{
	public enum InjuryCategoryEnum
	{
		Fatality,
		Injury,
		OccupationalIllness,
		OverseasDisease
	}
}
