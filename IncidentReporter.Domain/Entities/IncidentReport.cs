﻿using IncidentReporter.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.Entities
{
	public abstract class IncidentReport
	{
		public int Id { get; set; }

		#region Responsible person
		[Required(ErrorMessage = "Please complete the Title field")]
		public int TitleId { get; set; }

		[Required(ErrorMessage = "Please provide the first name of the Responsible Person")]
		public string FirstName { get; set; }

		[Required(ErrorMessage = "Please provide the last name of the Responsible Person")]
		public string LastName { get; set; }

		[Required(ErrorMessage = "Please provide the first line of the contact address for the Responsible Person")]
		public string AddressLine1 { get; set; }

		public string AddressLine2 { get; set; }

		[Required(ErrorMessage = "Please provide the town of the contact address for the Responsible Person")]
		public string Town { get; set; }

		[Required(ErrorMessage = "Please provide the postcode of the contact address for the Responsible Person")]
		public string Postcode { get; set; }

		[RegularExpression(@"\d{11}", ErrorMessage = "You must provide a valid 11 digit phone number")]
		public string Telephone { get; set; }

		[RegularExpression(@".+@.+\..+", ErrorMessage = "You must provide a valid email address")]
		public string Email { get; set; }
		#endregion

		#region Common incident properties
		[Required(ErrorMessage = "You must provide the date on which the incident occurred")]
		public DateTime DateOfIncident { get; set; }

		[Required(ErrorMessage = "You must provide a description of the incident occurred")]
		public string Description { get; set; }

		[Required(ErrorMessage = "You must provide the name of the person affected by the incident")]
		public string NameOfAffectedPerson { get; set; }

		/// <summary>
		/// For work injuries this refers to the affected person's Occupation as per RIDDOR
		/// For non-work injuries this refers to the affected person's Status (e.g. by stander, passenger, etc) as per RIDDOR
		/// </summary>
		[Required]
		public int AffectedPersonStatusId { get; set; }

		public DateTime CreatedOn { get; internal set; }
		#endregion

		#region State properties
		public ReportStatus ReportStatus { get; internal set; }
		public DateTime? SubmissionTimestamp { get; internal set; }
		#endregion

		#region Business logic methods
		public void MarkSubmitted()
		{
			// properties that define an object's state should not be modifiable outside the control of the object
			// the recommended way of doing this is to provide method access to ensure invariants are maintained
			SubmissionTimestamp = DateTime.Now;
			ReportStatus = ReportStatus.Submitted;
		}
		#endregion

	}
}
