﻿using IncidentReporter.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.Entities
{
	public class InjuryReport : IncidentReport
	{
		[RegularExpression("(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])", ErrorMessage = "You must provide the time of the injury in 24hr format")]
		[MaxLength(length: 5)]
		public string TimeOfIncident { get; set; }

		[Required(ErrorMessage = "Your must provide the location of the injury")]
		public string Location { get; set; }

		public int PeriodOfAbsence { get; set; }

		public int InjuryTypeId { get; set; }
	}
}
