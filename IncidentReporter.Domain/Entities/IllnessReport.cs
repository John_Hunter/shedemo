﻿using IncidentReporter.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Domain.Entities
{
	public class IllnessReport : IncidentReport
	{
		public int DiagnosisId { get; set; }

		public virtual InjuryType Diagnosis { get; set; }
	}
}
