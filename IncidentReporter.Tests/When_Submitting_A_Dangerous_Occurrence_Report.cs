﻿using IncidentReporter.Domain.Entities;
using IncidentReporter.Domain.Enums;
using IncidentReporter.Domain.Interfaces;
using IncidentReporter.Web.Controllers;
using IncidentReporter.Web.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentReporter.Tests
{
	[TestFixture]
	public class When_Submitting_A_Dangerous_Occurrence_Report
	{
		private IncidentReport _incidentReport;

		[SetUp]
		public void Setup()
		{
			var formModel = new DangerousOccurrenceFormModel
			{


				TitleId = 1,
				FirstName = "John",
				LastName = "Hunter",
				AddressLine1 = "123 Some Street",
				AddressLine2 = "",
				Town = "Anytown",
				Postcode = "AA12 3BZ",
				Telephone = "01234567890",
				Email = "my.name@somewhere.org",
				DateOfIncident = DateTime.Now,
				Description = "Dangerous Occurrence",
				NameOfAffectedPerson = "John Doe",
				AffectedPersonStatusId = 1,
				TimeOfIncident = "13:45",
				Location = "The bonnie, bonnie banks of Loch Lomond",
				CauseOfDangerousOccurrence = "Falling equipment"

			};

			_incidentReport = new DangerousOccurrenceReport
			{
				Id=1,
				TitleId  = 1,
				FirstName  = "John",
				LastName  = "Hunter",
				AddressLine1  = "123 Some Street",
				AddressLine2  = "",
				Town  = "Anytown",
				Postcode  = "AA12 3BZ",
				Telephone  = "01234567890",
				Email  = "my.name@somewhere.org",
				DateOfIncident = DateTime.Now,
				Description = "Dangerous Occurrence",
				NameOfAffectedPerson = "John Doe",
				AffectedPersonStatusId = 1,
				TimeOfIncident = "13:45",
				Location = "The bonnie, bonnie banks of Loch Lomond",
				CauseOfDangerousOccurrence = "Falling equipment"
			};
			var repo = new Mock<IDataContext>();

			repo.Setup(r => r.Get(It.IsAny<int>())).Returns(_incidentReport);
			var controller = new DangerousOccurrenceController(repo.Object);
			controller.ControllerContext = new System.Web.Mvc.ControllerContext();
			controller.ControllerContext.Controller = controller;
			controller.Edit(formModel, string.Empty, "Submit");
		}

		[Test]
		public void The_Status_Is_Set_To_Submitted()
		{
			Assert.AreEqual(ReportStatus.Submitted, _incidentReport.ReportStatus);
		}

		[Test]
		public void The_Submission_Timestamp_Is_Set()
		{
			Assert.IsNotNull(_incidentReport.SubmissionTimestamp);
		}
	}
}
